class Board
  attr_reader :grid
  # :m miss, :s ship, :x hit, nil empty space
  
  def initialize(grid=nil)
    @grid = grid.nil? ? self.class.default_grid : grid
    @empty = []
    # track empty coordinates
    @grid.each do |row|
      empty_row = []
      row.each.with_index { |c, i| empty_row << i if c.nil? }
      @empty << empty_row
    end
  end
  
  def [](pos)
    @grid[pos[0]][pos[1]]
  end
  
  def count
    @grid.reduce(0) { |acc, row| acc + row.count(:s) }
  end
  
  def empty?(pos=nil)
    if pos.nil?
      count == 0
    else
      @grid[pos[0]][pos[1]].nil?
    end
  end
    
  def full?
    @grid.all? { |row| row.none? { |c| c == nil } }
  end
  
  def attack(pos)
    @grid[pos[0]][pos[1]] = :x
  end
  
  def place_random_ship
    raise StandardError, "Can't place ship because board is already full" if full?
    rn = Random.new
    row = []
    y, x = nil
    while row.empty?
      y = rn.rand(0...@empty.length)
      row = @empty[y]
      x = row[rn.rand(0...row.length)] unless row.empty?
    end
    @empty[y].delete(x)
    @grid[y][x] = :s
  end
  
  def won?
    count == 0
  end
  
  def self.default_grid
    Array.new(10) { Array.new(10) }  
  end
end
