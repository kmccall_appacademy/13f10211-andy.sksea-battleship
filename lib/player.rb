class HumanPlayer
  
  def get_play
    puts "Give coordinates to attack: "
    coords = gets.chomp.split(", ").map { |i| i.to_i }
    [coords[0], coords[1]]
  end
  
end
